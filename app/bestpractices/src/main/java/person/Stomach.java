package person;

import food.Food;

import java.util.ArrayList;
import java.util.List;

public class Stomach {

    private List<Food> eatenFood;

    public Stomach() {
        eatenFood = new ArrayList<>();
    }

    public void digest(Food food) {
        eatenFood.add(food);
    }

    public List<Food> getEatenFood() {
        return eatenFood;
    }
}
