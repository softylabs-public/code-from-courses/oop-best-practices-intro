package person;

public class DigestiveSystem {

    private Stomach stomach;

    public DigestiveSystem() {
        stomach = new Stomach();
    }

    public Stomach getStomach() {
        return stomach;
    }
}
