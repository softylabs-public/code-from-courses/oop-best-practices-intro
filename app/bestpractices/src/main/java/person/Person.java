package person;

import house.House;

public class Person {

    private String firstName;
    private String lastName;
    private int height;
    private int birthYear;
    private Sex sex;
    private DigestiveSystem digestiveSystem = new DigestiveSystem();

    public Person(String firstName, String lastName, int height, int birthYear, Sex sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.height = height;
        this.birthYear = birthYear;
        this.sex = sex;
    }

    public DigestiveSystem getDigestiveSystem() {
        return digestiveSystem;
    }

    public void registerIn(House house) {
        house.addResident(this);
    }
}
