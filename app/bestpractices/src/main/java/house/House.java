package house;

import person.Person;

import java.util.ArrayList;
import java.util.List;

public class House {

    private List<Person> residents;

    public House() {
        residents = new ArrayList<>();
    }

    public List<Person> getResidents() {
        return residents;
    }

    public void addResident(Person person) {
        residents.add(person);
    }
}
