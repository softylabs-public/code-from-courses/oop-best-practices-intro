package person;

import food.Carrot;
import house.House;
import org.junit.Test;

import static org.junit.Assert.*;

public class PersonTest {

    @Test
    public void createNewPerson() {
        Person person = getPerson();

        assertNotNull(person);
    }

    @Test
    public void personShouldBeAbleToDigestFood() {
        Person person = getPerson();

        person.getDigestiveSystem().getStomach().digest(new Carrot());

        assertFalse(person.getDigestiveSystem().getStomach().getEatenFood().isEmpty());
    }

    @Test
    public void registerInHouse() {
        House house = new House();
        Person person = getPerson();

        person.registerIn(house);

        assertTrue(house.getResidents().contains(person));
    }

    private Person getPerson() {
        return new Person("John", "Doe", 180, 1980, Sex.MALE);
    }
}
